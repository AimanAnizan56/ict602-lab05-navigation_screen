import 'package:flutter/material.dart';
import '/second_screen.dart';

void main() {
  runApp(MaterialApp(
    title:'Navigation basics',
    home: FirstScreen(),
  ));
}

class FirstScreen extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Screen')
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Next Screen'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SecondScreen()),
            );
          },
        )
      ),
    );
  }
}