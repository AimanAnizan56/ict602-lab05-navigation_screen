import 'package:flutter/material.dart';
import '/main.dart';

class SecondScreen extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second Screen'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('First Screen'),
          onPressed: () {
            Navigator.pop(
              context,
              MaterialPageRoute(builder: (context) => FirstScreen())
            );
          },
        ),
      )
    );
  }
}